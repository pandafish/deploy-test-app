
<h1 align="center" style="margin-top: 30px;">HeY tHeRe<br> MeRlIjN</h1>

<p align="center" style="margin-top: 70px;"><img src="2zerkp.jpg"></p>


<?php

$_CONFIG = include_once '_meta_config.php';

$connection = true;

try {
    $db = new PDO('mysql:dbname='.$_CONFIG['mysql_database'].';host='.$_CONFIG['mysql_host'], $_CONFIG['mysql_user'], $_CONFIG['mysql_pw']);
} catch (PDOException $e) {
    $connection = false;
}

?>


<?php if($connection): ?>
    <p align="center" style="margin-top: 50px; color: mediumseagreen">
        Connected to the database. <br />
        <span style="font-size: 3em;line-height: 2em;">🎉</span>
    </p>
<?php else: ?>
    <p align="center" style="margin-top: 50px; color: orangered">
        Unable to connect to the database. <br />
        <span style="font-size: 3em;line-height: 2em;">😨</span>
    </p>
<?php endif; ?>
